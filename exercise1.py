
user_str = input().strip()
# Первый способо решения. Простой с применением методом обработки строк.
print('Ниже представлен первый способ решения:')
str_list = user_str.split(' ')
index = 1
for s in str_list:
    if (s==''):
        continue
    print(index, s[0:10])
    index+=1

#Второй способ решения без применения строковых методов.
print('Ниже представлен второй способ решения:')
index = 1
tmp_str = ''
if (user_str!=''):
    for i in user_str:
        if i==' ' and tmp_str!='':
            print(index,tmp_str[0:10])
            index+=1
            tmp_str=''
        elif i==' ' and tmp_str=='':
            continue
        elif i!=' ':
            tmp_str = tmp_str+i
    print(index, tmp_str)