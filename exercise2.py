class Stationery():
    def __init__(self, title=None):
        self._title = title

    def draw(self):
        print('Запуск отрисовки')

class Pen(Stationery):
    def draw(self):
        print('Запуск отрисовки ручкой')

class Pencil(Stationery):
    def draw(self):
        print('Запуск отрисовки карандашом')

class Handel(Stationery):
    def draw(self):
        print('Запуск отрисовки кистью')

stationary = Stationery()
# Создаем объекты классов
pen = Pen()
pencil = Pencil()
handel = Handel()

#Проверяем работу методов
pen.draw()
pencil.draw()
handel.draw()