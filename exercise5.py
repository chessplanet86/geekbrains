class OwnError(Exception):
    def __init__(self, txt):
        self.txt = txt
    def __str__(self):
        return f'{self.txt}'

# inp_data = int(input('Первое число ')) # Здесь не нужно оборачивать input() в int(), идет дублирование на 11 строчке
# inp_data2 = int(input('Второе число ')) # Здесь не нужно оборачивать input() в int(), идет дублирование на 12 строчке

# Правильный вариант
inp_data = input('Первое число ')
inp_data2 = input('Второе число ')


try:
    inp_data = int(inp_data)
    inp_data2 = int(inp_data2)

    if inp_data2==0: #В случае деления на ноль вызываем пользовательское исключение
        raise OwnError('Ошибка деления на ноль. На ноль делить нельзя.') 

    z = inp_data / inp_data2
    

except ZeroDivisionError: 
    #Данное исключение срабатывает в случае деления на ноль. 
    #Класс OwnError не принимает здесь никакого участия в генерации этой ошибки
    #Вызов данного исключения не обязателен
    c = OwnError('Была ошибка')  
except ValueError:
    print("Вы ввели не число")
except OwnError as err:
    print(err)
else:
    print(f"Все хорошо. Ваше число: {z}")